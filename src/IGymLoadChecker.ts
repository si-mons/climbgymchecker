export interface IGymLoadChecker {
	getName(): string;
	getLoad(): Promise<string>;
}
