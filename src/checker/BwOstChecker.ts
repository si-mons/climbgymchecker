import { IGymLoadChecker } from 'src/IGymLoadChecker';
import { Boulderwelt } from './Boulderwelt';

export class BwOstChecker extends Boulderwelt implements IGymLoadChecker {
	private static URL =
		'https://www.boulderwelt-muenchen-ost.de/wp-admin/admin-ajax.php?action=cxo_get_crowd_indicator';

	getName(): string {
		return 'Boulderwelt Ost';
	}

	getLoad(): Promise<string> {
		return super.getLoad(BwOstChecker.URL);
	}
}
