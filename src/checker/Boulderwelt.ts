import axios from 'axios';

export abstract class Boulderwelt {
	getLoad(url: string): Promise<string> {
		return new Promise(async (resolve, reject) => {
			// load document from url
			const document = await axios.get(url);
			// resolve percentage
			resolve(document.data.percent);
		});
	}
}
