import { IGymLoadChecker } from 'src/IGymLoadChecker';
import { Boulderwelt } from './Boulderwelt';

export class BwWestChecker extends Boulderwelt implements IGymLoadChecker {
	private static URL =
		'https://www.boulderwelt-muenchen-west.de/wp-admin/admin-ajax.php?action=cxo_get_crowd_indicator';

	getName(): string {
		return 'Boulderwelt West';
	}

	getLoad(): Promise<string> {
		return super.getLoad(BwWestChecker.URL);
	}
}
