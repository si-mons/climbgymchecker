import { IGymLoadChecker } from 'src/IGymLoadChecker';
import axios from 'axios';
import cheerio from 'cheerio';

export class EinsteinChecker implements IGymLoadChecker {
	private static URL =
		'https://www.boulderado.de/boulderadoweb/gym-clientcounter/index.php?mode=get&token=eyJhbGciOiJIUzI1NiIsICJ0eXAiOiJKV1QifQ.eyJjdXN0b21lciI6IkVpbnN0ZWluTSJ9.uH9xRoVykz5fzofHc-JGigeHreaeTayel49o3FR6cNA&ampel=0';
	getName(): string {
		return 'Einstein Boulderhalle';
	}
	getLoad(): Promise<string> {
		return new Promise(async (resolve, reject) => {
			// load document from url
			const document = await axios.get(EinsteinChecker.URL);
			const $ = cheerio.load(document.data);
			// get information
			const cnt = parseInt($('.actcounter-content').text());
			const free = parseInt($('.freecounter-content').text());
			const all = cnt + free;
			// calc load
			const load = ((cnt / all) * 100).toFixed(0);
			// resolve
			resolve(load);
		});
	}
}
