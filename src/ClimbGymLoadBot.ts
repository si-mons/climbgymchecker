import TelegramAPI from 'telegram-bot-api';
import { Phrases } from './Phrases';
import { IGymLoadChecker } from './IGymLoadChecker';

export class ClimbGymLoadBot {
	telegramConnector: any;
	checkerMap: Map<string, IGymLoadChecker>;

	constructor(telegramToken: string, checkerMap: Map<string, IGymLoadChecker>) {
		// init connector
		this.telegramConnector = new TelegramAPI({
			token: telegramToken,
			updates: { enabled: true },
		});
		this.checkerMap = checkerMap;
	}

	public listen() {
		this.telegramConnector.on('message', this.onMessage.bind(this));
		console.info(
			`Bot listening for messages, ${this.checkerMap.size} checker(s) set`
		);
	}

	public sendText(chatId: string, text: string) {
		this.telegramConnector.sendMessage({
			chat_id: chatId,
			text: text,
			parse_mode: 'MarkdownV2',
		});
	}

	private onMessage(message: any) {
		// log
		console.info(
			`Message received from ${message.from.id || message.from.username} (${
				message.from.first_name
			}): ${message.text}`
		);
		// get mandatory chat id
		const chatId = message.chat.id;
		if (!chatId) {
			return;
		}
		// get info
		const text = (message.text as string) || '';
		const parts = text.split(' ');
		const cmd = parts[0] || '';
		// only messages starting with / are considered
		if (!cmd.startsWith('/')) {
			return;
		}
		// check cmd
		switch (cmd) {
			case '/start':
				this.sendText(chatId, Phrases.START);
				break;
			default:
				this.onCheck(chatId, cmd.substring(1));
		}
	}

	public listCheckers() {
		let r = '';
		this.checkerMap.forEach((val, key) => {
			r = r + `*/${key}* \\- ${val.getName()}\n`;
		});

		return r.trim();
	}

	private onCheck(chatId: string, param: string) {
		// check if gym key exists
		var checker = this.checkerMap.get(param) as IGymLoadChecker;
		if (!checker) {
			// notify key not found send possible keys
			this.sendText(chatId, Phrases.GYM_NOT_FOUND + this.listCheckers());

			return;
		}
		// get load
		checker
			.getLoad()
			.then(
				((load: string) => {
					// send load
					this.sendText(chatId, `${checker.getName()}: *${load}%*`);
				}).bind(this)
			)
			.catch(
				(() => {
					this.sendText(chatId, Phrases.ERROR);
				}).bind(this)
			);
	}
}
