import { ClimbGymLoadBot } from './src/ClimbGymLoadBot';
import { IGymLoadChecker } from './src/IGymLoadChecker';
import { EinsteinChecker } from './src/checker/EinsteinChecker';
import { BwWestChecker } from './src/checker/BwWestChecker';
import { BwOstChecker } from './src/checker/BwOstChecker';

// bot token
const token = '1178762969:AAFZEPH3j8lH5fdvLPUXubVGafq7E8tICAo';

// gym checkers
const checkerMap: Map<string, IGymLoadChecker> = new Map([
	['einstein', new EinsteinChecker()],
	['west', new BwWestChecker()],
	['ost', new BwOstChecker()],
]);

// init bot
new ClimbGymLoadBot(token, checkerMap).listen();
